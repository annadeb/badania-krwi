﻿using DynamicDataDisplay.Markers.DataSources.ValueConverters;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.Charts;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WynikiBadan
{
    /// <summary>
    /// Interaction logic for AnalizaWynikow.xaml
    /// </summary>
    public partial class AnalizaWynikow : Window
    {
        private Badania badanie = new Badania();
        private Pacjenci pacjent = new Pacjenci();
        public AnalizaWynikow(Pacjenci pacjent)
        {
            InitializeComponent();
            this.pacjent = new Pacjenci();
            this.pacjent.IDpacjenta = pacjent.IDpacjenta;
            this.pacjent.Imie = pacjent.Imie;
            this.pacjent.Nazwisko = pacjent.Nazwisko;
            this.pacjent.Pesel = pacjent.Pesel;
            this.pacjent.Haslo = pacjent.Haslo;
            //this.pacjent.Badania = pacjent.Badania;
            var lista = typeof(Badania).GetMembers();
            MemberInfo[] lista2 = new MemberInfo[lista.Length-17];// = lista.ToArray();
            for (int i = 17; i < lista.Length; i++)
            {
                lista2[i-17] = lista[i];
            }
            cbCombo.ItemsSource = lista2;

            //http://www.altcontroldelete.pl/artykuly/wpf-tutorial-bindowanie/
        }

        private void bWstecz_Click(object sender, RoutedEventArgs e)
        {
            WyborWidoku win2 = new WyborWidoku(pacjent);
            win2.Show();
            this.Close();
        }

        private void cbCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //string parametr = cbCombo.SelectionBoxItemStringFormat;
            //string par = cbCombo.SelectedValue.ToString();

            string par2 = cbCombo.SelectedValue.ToString();
            var cos = par2.Split(' ');
            string parametr = cos[1]; //"System.Nullable`1[System.Double] "
            try
            {
                string connString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=C:\Users\Anna\Desktop\Studia\SEMESTR_5\Obliczenia Inżynierskie - Projekt\VisualProject\WynikiBadan\WynikiBadan\WynikiBadanDb.mdf; Integrated Security=True";

                string query = "select * from Badania where IDpacjenta=" + pacjent.IDpacjenta;

                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter adapter;
                DataSet dataSet;
                DataTable dt = new DataTable();
                adapter = new SqlDataAdapter(cmd);
                dataSet = new DataSet();
                // create data adapter

                // this will query your database and return the result to your datatable
                adapter.Fill(dataSet, "Badania");
                //BindControls();
                dt = dataSet.Tables["Badania"];
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Brak danych do wizualizacji.","Uwaga!",MessageBoxButton.OK,MessageBoxImage.Information);
                }
                else
                {
                    List<double> listaWart = new List<double>();
                    List<DateTime> listaDat = new List<DateTime>();
                    List<double> listaDatDouble = new List<double>();
                    List<double> listalicz = new List<double>();
                    double licznik = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        double wartosc = (double)(dr[parametr]);
                        DateTime data = (DateTime)(dr["DataBadania"]);                 
                        if (wartosc != -1)
                        {
                            listaWart.Add(wartosc);
                            listaDat.Add(data);
                            string datastr = data.ToShortDateString().Remove(5,5).Replace('.',',');
                            double datadouble = double.Parse(datastr);
                            listaDatDouble.Add(datadouble);
                            Point pkt = new Point(licznik, wartosc);
                            listalicz.Add(licznik);
                        }
                        licznik++;
                    }
                    int N = listalicz.Count;
                    double[] c = new double[N];
                    double[] d = new double[N];

                    Random rand = new Random();
                    for (int i = 0; i < N; i++)
                    {
                        c[i] = 0.4;
                        d[i] = 20* rand.NextDouble();
                    }
                    
                    circles.PlotColorSize(listalicz, listaWart, c, d); 
 
                }
                conn.Close();
                adapter.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
