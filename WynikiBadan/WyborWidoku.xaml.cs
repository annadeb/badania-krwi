﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WynikiBadan
{
    /// <summary>
    /// Interaction logic for WyborWidoku.xaml
    /// </summary>
    public partial class WyborWidoku : Window
    {
        private Pacjenci pacjent = new Pacjenci();
        public WyborWidoku(Pacjenci pacjent)
        {
            InitializeComponent();
            this.pacjent = new Pacjenci();
            this.pacjent.IDpacjenta = pacjent.IDpacjenta;
            this.pacjent.Imie = pacjent.Imie;
            this.pacjent.Nazwisko = pacjent.Nazwisko;
            this.pacjent.Pesel = pacjent.Pesel;
            this.pacjent.Haslo = pacjent.Haslo;
            //this.pacjent.Badania = pacjent.Badania;
        }

        private void bDodajbad_Click(object sender, RoutedEventArgs e)
        {
            Formularz win2 = new Formularz(pacjent);
            win2.Show();
            this.Close();
        }

        private void bAnaliza_Click(object sender, RoutedEventArgs e)
        {
            AnalizaWynikow win2 = new AnalizaWynikow(pacjent);
            win2.Show();
            this.Close();
        }

        private void bWyloguj_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            win2.Show();
            this.Close();
        }
    }
}
