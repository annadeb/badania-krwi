﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading;
using System.Data;
using System.Reflection;

namespace WynikiBadan
{
    /// <summary>
    /// Interaction logic for Formularz.xaml
    /// </summary>
    public partial class Formularz : Window
    {
        private Badania badanie = new Badania();
        private Pacjenci pacjent = new Pacjenci();
        public Formularz(Pacjenci pacjent)
        {
            InitializeComponent();
            this.pacjent = new Pacjenci();
            this.pacjent.IDpacjenta = pacjent.IDpacjenta;
            this.pacjent.Imie = pacjent.Imie;
            this.pacjent.Nazwisko = pacjent.Nazwisko;
            this.pacjent.Pesel = pacjent.Pesel;
            this.pacjent.Haslo = pacjent.Haslo;
            //this.pacjent.Badania = pacjent.Badania;
        }

        private void bZatwierdz_Click(object sender, RoutedEventArgs e)
        {
            textBoxLeukocyty.Foreground = CzyZielonyElseCzerwony(ref badanie.leukocyty, textBoxLeukocyty.Text, textBoxLeukocyty.Foreground, 3.5, 10);
            CzyPusty(textBoxLeukocyty, ref badanie.leukocyty);
            textBoxErytrocyty.Foreground = CzyZielonyElseCzerwony(ref badanie.erytrocyty, textBoxErytrocyty.Text, textBoxErytrocyty.Foreground, 4.5, 5.7);
            CzyPusty(textBoxErytrocyty, ref badanie.erytrocyty);
            textBoxHemoglobina.Foreground = CzyZielonyElseCzerwony(ref badanie.hemoglobina, textBoxHemoglobina.Text, textBoxHemoglobina.Foreground, 12, 16.8);
            CzyPusty(textBoxHemoglobina, ref badanie.hemoglobina);
            textBoxHematokryt.Foreground = CzyZielonyElseCzerwony(ref badanie.hematokryt, textBoxHematokryt.Text, textBoxHematokryt.Foreground, 40, 49.5);
            CzyPusty(textBoxHematokryt, ref badanie.hematokryt);
            textBoxMCV.Foreground = CzyZielonyElseCzerwony(ref badanie.mCV, textBoxMCV.Text, textBoxMCV.Foreground, 83, 103);
            CzyPusty(textBoxMCV, ref badanie.mCV);
            textBoxMCH.Foreground=CzyZielonyElseCzerwony(ref badanie.mCH, textBoxMCH.Text, textBoxMCH.Foreground, 28, 34);
            CzyPusty(textBoxMCH, ref badanie.mCH);
            textBoxMCHC.Foreground=CzyZielonyElseCzerwony(ref badanie.mCHC, textBoxMCHC.Text, textBoxMCHC.Foreground, 32, 36);
            CzyPusty(textBoxMCHC, ref badanie.mCHC);
            textBoxPlytkikrwi.Foreground=CzyZielonyElseCzerwony(ref badanie.plytkikrwi, textBoxPlytkikrwi.Text, textBoxPlytkikrwi.Foreground, 125, 400);
            CzyPusty(textBoxPlytkikrwi, ref badanie.plytkikrwi);
            textBoxRDWCV.Foreground=CzyZielonyElseCzerwony(ref badanie.rDWCV, textBoxRDWCV.Text, textBoxRDWCV.Foreground, 11.5, 14.5);
            CzyPusty(textBoxRDWCV, ref badanie.rDWCV);
            textBoxPDW.Foreground=CzyZielonyElseCzerwony(ref badanie.pDW, textBoxPDW.Text, textBoxPDW.Foreground, 9.8, 16.1);
            CzyPusty(textBoxPDW, ref badanie.pDW);
            textBoxMPV.Foreground=CzyZielonyElseCzerwony(ref badanie.mPV, textBoxMPV.Text, textBoxMPV.Foreground, 9, 12.6);
            CzyPusty(textBoxMPV, ref badanie.mPV);
            textBoxPCT.Foreground=CzyZielonyElseCzerwony(ref badanie.pCT, textBoxPCT.Text, textBoxPCT.Foreground, 0.14, 0.36);
            CzyPusty(textBoxPCT, ref badanie.pCT);
            textBoxProcNeutro.Foreground=CzyZielonyElseCzerwony(ref badanie.procNeutro, textBoxProcNeutro.Text, textBoxProcNeutro.Foreground, 45, 70);
            CzyPusty(textBoxProcNeutro, ref badanie.procNeutro);
            textBoxProcBazo.Foreground=CzyZielonyElseCzerwony(ref badanie.procBazo, textBoxProcBazo.Text, textBoxProcBazo.Foreground, 0, 1);
            CzyPusty(textBoxProcBazo, ref badanie.procBazo);
            textBoxProcLimfo.Foreground=CzyZielonyElseCzerwony(ref badanie.procLimfo, textBoxProcLimfo.Text, textBoxProcLimfo.Foreground, 20, 45);
            CzyPusty(textBoxProcLimfo, ref badanie.procLimfo);
            textBoxProcMono.Foreground=CzyZielonyElseCzerwony(ref badanie.procMono, textBoxProcMono.Text, textBoxProcMono.Foreground, 4, 12);
            CzyPusty(textBoxProcMono, ref badanie.procMono);
            textBoxProcEozyno.Foreground=CzyZielonyElseCzerwony(ref badanie.procEozyno, textBoxProcEozyno.Text, textBoxProcEozyno.Foreground, 0, 5);
            CzyPusty(textBoxProcEozyno, ref badanie.procEozyno);
            textBoxLiczNeutro.Foreground=CzyZielonyElseCzerwony(ref badanie.liczNeutro, textBoxLiczNeutro.Text, textBoxLiczNeutro.Foreground, 1.8, 7.7);
            CzyPusty(textBoxLiczNeutro, ref badanie.liczNeutro);
            textBoxLiczBazo.Foreground=CzyZielonyElseCzerwony(ref badanie.liczBazo, textBoxLiczBazo.Text, textBoxLiczBazo.Foreground, 0, 0.2);
            CzyPusty(textBoxLiczBazo, ref badanie.liczBazo);
            textBoxLiczLimfo.Foreground=CzyZielonyElseCzerwony(ref badanie.liczLimfo, textBoxLiczLimfo.Text, textBoxLiczLimfo.Foreground, 1, 5);
            CzyPusty(textBoxLiczLimfo, ref badanie.liczLimfo);
            textBoxLiczMono.Foreground=CzyZielonyElseCzerwony(ref badanie.liczMono, textBoxLiczMono.Text, textBoxLiczMono.Foreground, 0, 0.8);
            CzyPusty(textBoxLiczMono, ref badanie.liczMono);
            textBoxLiczEozyno.Foreground=CzyZielonyElseCzerwony(ref badanie.liczEozyno, textBoxLiczEozyno.Text, textBoxLiczEozyno.Foreground, 0, 0.45);
            CzyPusty(textBoxLiczEozyno, ref badanie.liczEozyno);

            string messageStr = "";
            if (badanie.erytrocyty <4.5 && badanie.erytrocyty >0 && badanie.hemoglobina<12 && badanie.hemoglobina > 0 && badanie.hematokryt < 40 && badanie.hematokryt > 0)
            {
                messageStr = "Obniżenie poziomu erytrocytów, hemoglobiny \ni hematokrytu może być objawem anemii.";
                MessageBox.Show(messageStr, "Uwaga!", MessageBoxButton.OK,MessageBoxImage.Warning);
            }

        }



        private void textBoxLeukocyty_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxLeukocyty);
        }

        private void textBoxErytrocyty_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxErytrocyty);
        }

        private void textBoxHemoglobina_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxHemoglobina);
        }

        private void textBoxHematokryt_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxHematokryt);
        }

        private void textBoxMCV_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxMCV);
        }

        private void textBoxMCH_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxMCH);
        }

        private void textBoxMCHC_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxMCHC);
        }

        private void textBoxPlytkikrwi_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxPlytkikrwi);
        }

        private void textBoxRDWCV_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxRDWCV);
        }

        private void textBoxPDW_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxPDW);
        }

        private void textBoxMPV_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxMPV);
        }

        private void textBoxPCT_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxPCT);
        }

        private void textBoxProcNeutro_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxProcNeutro);
        }

        private void textBoxProcBazo_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxProcBazo);
        }

        private void textBoxProcLimfo_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxProcLimfo);
        }

        private void textBoxProcMono_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxProcMono);
        }

        private void textBoxProcEozyno_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxProcEozyno);
        }

        private void textBoxLiczNeutro_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxLiczNeutro);
        }

        private void textBoxLiczBazo_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxLiczBazo);
        }

        private void textBoxLiczLimfo_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxLiczLimfo);
        }

        private void textBoxLiczMono_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxLiczMono);
        }

        private void textBoxLiczEozyno_TextChanged(object sender, TextChangedEventArgs e)
        {
            CzyLiczba(ref textBoxLiczEozyno);
        }

        private void bDalej_Click(object sender, RoutedEventArgs e)
        {
            bZatwierdz_Click(bZatwierdz, new RoutedEventArgs());
            Thread.Sleep(2000);

            // string dbconnectionstring = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C: \Users\Anna\Desktop\Studia\SEMESTR_5\Obliczenia Inżynierskie -Projekt\VisualProject\WynikiBadan\WynikiBadan\WynikiBadanDb.mdf;Integrated Security=True";
            using (SqlConnection con = new SqlConnection())
            {
                try
                {
                    con.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=C:\Users\Anna\Desktop\Studia\SEMESTR_5\Obliczenia Inżynierskie - Projekt\VisualProject\WynikiBadan\WynikiBadan\WynikiBadanDb.mdf; Integrated Security=True";

                    SqlCommand polecenie = new SqlCommand();
                    polecenie.Connection = con;
                    badanie.DataBadania = DateTime.Today;
                    polecenie.CommandText = "Insert into Badania (IDpacjenta, DataBadania, leukocyty, erytrocyty,hemoglobina,hematokryt,mCV,mcH,mCHC,plytkikrwi,rDWCV,pDW,mPV,pCT,procNeutro,procBazo,procLimfo,procMono,procEozyno,liczNeutro,liczBazo,liczLimfo,liczMono,liczEozyno) values (" + pacjent.IDpacjenta + ", '" + badanie.DataBadania.ToString("MM/dd/yyyy") + "', " + badanie.leukocyty + ", " + badanie.erytrocyty + "," + badanie.hemoglobina + ", " + badanie.hematokryt + ", " + badanie.mCV + ", " + badanie.mCH + ", " + badanie.mCHC + ", " + badanie.plytkikrwi + "," + badanie.rDWCV + "," + badanie.pDW + "," + badanie.mPV + "," + badanie.pCT + "," + badanie.procNeutro + "," + badanie.procBazo + "," + badanie.procLimfo + "," + badanie.procMono + "," + badanie.procEozyno + "," + badanie.liczNeutro + "," + badanie.liczBazo + "," + badanie.liczLimfo + "," + badanie.liczMono + "," + badanie.liczEozyno + ")";
                    con.Open();
                    polecenie.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Dane zostały poprawnie zapisane.","Gratulacje!",MessageBoxButton.OK,MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(ex.Message,"Błąd",MessageBoxButton.OK,MessageBoxImage.Error);
                }
            }
           
        }


        private Brush CzyZielonyElseCzerwony(ref double? to, string tbnameDotText, Brush brush, double minvalue, double maxvalue)
        {
            if (!(tbnameDotText == string.Empty))
            {
                to = double.Parse(tbnameDotText.Replace('.', ','));
                if (to >= minvalue && to <= maxvalue)
                {
                    
                    return brush = Brushes.Green;
                }
                else
                {
                   return brush = Brushes.Red;
                }        
            }
            return brush;
        }
        private void CzyLiczba(ref TextBox tbText)
        {

            var czyliczba = tbText.Text.Replace('.', ',');
            double cos;
            if (double.TryParse(czyliczba, out cos))
            {
            }
            else if (czyliczba == "")
            {
            }
            else
            {
                MessageBox.Show("Należy podać prawidłowe dane.", "Błąd", MessageBoxButton.OK,MessageBoxImage.Error);
                tbText.Text = string.Empty;
                return;
            }
        }
        public double? CzyPusty(TextBox tbText, ref double? param)
        {
            if (tbText.Text == "")
            {
                param = -1.0;
            }
            return param;
        }

        private void bWstecz_Click(object sender, RoutedEventArgs e)
        {
            WyborWidoku win2 = new WyborWidoku(pacjent);
            win2.Show();
            this.Close();
        }
    }
}
