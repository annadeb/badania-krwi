﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WynikiBadan
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       public Pacjenci pacjent = new Pacjenci();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void bDalej_Click(object sender, RoutedEventArgs e)
        {
            WynikiBadanDataSet db = new WynikiBadanDataSet();
            
            if (tbPesel.Text.All(char.IsDigit) && tbPesel.Text.Length == 11)
            {
                pacjent.Pesel = tbPesel.Text;
            }
            else
            {
                MessageBox.Show("W polu PESEL należy podać prawidłowe dane.","Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                tbPesel.Text = tbPesel.Text.Remove(0, tbPesel.Text.Length);
                return;
            }
            if (passwordBox.Password == string.Empty)
            {
                MessageBox.Show("Pole hasło nie może być puste", "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                pacjent.Haslo = passwordBox.Password.ToString();
                var userDetails = db.Pacjenci.Where(x => x.Pesel == tbPesel.Text && x.Haslo == passwordBox.Password).FirstOrDefault();

                using (SqlConnection con = new SqlConnection())
                {
                    try
                    {
                        string connString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=C:\Users\Anna\Desktop\Studia\SEMESTR_5\Obliczenia Inżynierskie - Projekt\VisualProject\WynikiBadan\WynikiBadan\WynikiBadanDb.mdf; Integrated Security=True";

                        string query = "select * from Pacjenci where Pesel='"+ tbPesel.Text +"' AND Haslo ='" + passwordBox.Password.ToString() + "'"; 

                        SqlConnection conn = new SqlConnection(connString);
                        SqlCommand cmd = new SqlCommand(query, conn);
                        SqlDataAdapter adapter;
                        DataSet dataSet;
                        DataTable dt = new DataTable();
                        adapter = new SqlDataAdapter(cmd);
                        dataSet = new DataSet();
                        // create data adapter

                        // this will query your database and return the result to your datatable
                        adapter.Fill(dataSet, "Pacjenci");
                        //BindControls();
                        dt = dataSet.Tables["Pacjenci"];
                        if (dt.Rows.Count==0)
                        {
                            MessageBox.Show("W bazie nie istnieje taki pacjent. \n Sprawdź poprawność danych lub zarejestruj się.", "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        else
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                //MessageBox.Show(dr["IDpacjenta"].ToString());
                                pacjent.IDpacjenta = (int)dr["IDpacjenta"];
                                pacjent.Imie = dr["Imie"].ToString();
                                pacjent.Nazwisko = dr["Nazwisko"].ToString();
                               // pacjent.Badania = (ObservableCollection<Badania>)dr["Badania"];
                            }
                            //Formularz win2 = new Formularz(pacjent);
                            WyborWidoku win2 = new WyborWidoku(pacjent);
                            win2.Show();
                            this.Close();
                        }
                       


                        //int rowNum = 1; // row number
                        //string columnName = "IDpacjenta";  // database table column name
                        //dt.Rows[rowNum][columnName].ToString();

                        conn.Close();
                        adapter.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
               // Pacjenci pacjent = new Pacjenci();
               
            }
        }

        private void tbImie_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox txtBox = sender as TextBox;
            if (txtBox.Text == "watermark...")
                txtBox.Text = string.Empty;
        }

        private void tbPesel_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void bZarejestruj_Click(object sender, RoutedEventArgs e)
        {
            ////https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/sql/linq/how-to-query-for-information
            //Northwnd db = new Northwnd(@"c:\northwnd.mdf");

            //// Query for customers in London.
            //IQueryable<Customer> custQuery =
            //    from cust in db.Customers
            //    where cust.City == "London"
            //    select cust;
            Rejestracja win2 = new Rejestracja();
            win2.Show();
            this.Close();
        }
    }
}
