﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WynikiBadan
{
    /// <summary>
    /// Interaction logic for Rejestracja.xaml
    /// </summary>
    public partial class Rejestracja : Window
    {
        public Pacjenci pacjent = new Pacjenci();

        public Rejestracja()
        {
            InitializeComponent();
        }

        private void bPowrót_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            win2.Show();
            this.Close();
        }

        private void bZapisz_Click(object sender, RoutedEventArgs e)
        {
            
            if (tbImie.Text == string.Empty)
            {
                ToolTip tool = new ToolTip();

                MessageBox.Show("Pole imię nie może być puste", "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
                pacjent.Imie = tbImie.Text;
            if (tbNazwisko.Text == string.Empty)
            {
                MessageBox.Show("Pole nazwisko nie może być puste", "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
                pacjent.Nazwisko = tbNazwisko.Text;

            if (tbPesel.Text.All(char.IsDigit) && tbPesel.Text.Length == 11)
            {
                pacjent.Pesel = tbPesel.Text;
            }
            else
            {
                MessageBox.Show("W polu PESEL należy podać prawidłowe dane.","Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                tbPesel.Text = tbPesel.Text.Remove(0, tbPesel.Text.Length);
                return;
            }
            if (passwordBox.Password == string.Empty)
            {
                MessageBox.Show("Pole hasło nie może być puste", "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                pacjent.Haslo = passwordBox.Password.ToString();
            }
            if (passwordBox2.Password == string.Empty)
            {
                MessageBox.Show("Musisz powtórzyć hasło", "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                pacjent.Haslo = passwordBox.Password.ToString();
            }
            if (passwordBox.Password != passwordBox2.Password)
            {
                MessageBox.Show("Hasła różnią się", "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                pacjent.Haslo = passwordBox.Password.ToString();
                using (SqlConnection con = new SqlConnection())
                {
                    try
                    {
                        con.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=C:\Users\Anna\Desktop\Studia\SEMESTR_5\Obliczenia Inżynierskie - Projekt\VisualProject\WynikiBadan\WynikiBadan\WynikiBadanDb.mdf; Integrated Security=True";

                        SqlCommand polecenie = new SqlCommand();
                        polecenie.Connection = con;

                        polecenie.CommandText = "Insert into Pacjenci (Imie, Nazwisko, Pesel, Haslo) values ('" + pacjent.Imie+ "','"+ pacjent.Nazwisko+ "','" + pacjent.Pesel+ "','" + pacjent.Haslo + "')";

                        con.Open();
                        polecenie.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
                MessageBox.Show("Dane zostały poprawnie zapisane.","Gratulacje!", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
    }
}
