﻿CREATE TABLE [dbo].[Table]
(
	[IDpacjenta] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Imie] NCHAR(50) NOT NULL, 
    [Nazwisko] NCHAR(50) NOT NULL, 
    [Pesel] NCHAR(50) NOT NULL, 
    [Haslo] NCHAR(50) NOT NULL
)
