﻿CREATE TABLE [dbo].[Badania]
(
	[IDbadania] INT NOT NULL PRIMARY KEY, 
    [IDpacjenta] INT NOT NULL, 
    [DataBadania] DATE NOT NULL, 
    CONSTRAINT [IDpacjenta] FOREIGN KEY ([IDpacjenta]) REFERENCES [Pacjenci]([IDpacjenta])
)
